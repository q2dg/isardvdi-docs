# Despliegues

## Gestionar

Los despliegues creados por los usuarios se pueden gestionar aquí. La información sobre los escritorios y la visibilidad se muestran a la mesa y en los detalles de los parámetros del escritorio y se pueden ver los usuarios y grupos desplegados.

![](deployments.images/deployment1.png)

## Eliminar

!!! danger "Acción no reversible"

    Actualmente el borrado de un despliegue y sus escritorios no se puede recuperar.
    Con la entrada de la MR de [papelera de reciclaje](https://gitlab.com/isard/isardvdi/-/merge_requests/1692)
    se podrán recuperar durante un tiempo predefinido.

El icono de supresión a la derecha de cada fila permite suprimir el despliegue, por lo tanto los escritorios que pertenecen.