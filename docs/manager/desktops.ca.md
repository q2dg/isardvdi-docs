# Escriptoris

Per anar a la secció "Desktops", es prem el botó ![Administration](../admin/users.ca.images/users1.png)

![pantalla de la pàgina inicial](../admin/users.ca.images/users2.png)

I a la secció "Desktops"

![barra lateral d'administració](./domains.images/domains2.png)

En aquesta secció es poden veure tots els escriptoris que s'han creat de totes les categories.

Al fer clic a la icona ![[+]](./domains.images/domains5.png), es mostrarà més informació sobre l'escriptori.

* Status detailed info: aquí es mostra l'estat de l'escriptori. En cas d'escriptori fallat, es mostrarà un missatge d'error que pot ajudar a identificar el problema

* Description: descripció de l'escriptori

* Hardware: com el seu nom indica, és el maquinari que s'ha assignat a aquest escriptori

* Storage: indica l'UUID del disc associat i el seu ús

* Media: indica que els mitjans associats a l'escriptori, si n'hi ha cap

![Detalls de l'escriptori](./domains.images/domains6.png)

### Accions globals

A la part superior de la taula tenim un desplegable amb diverses accions. Aquestes accions s'aplicaran a tots els escriptoris seleccionats.

![Desplegable d'accions globals](./domains.images/domains54.png)

Per seleccionar un escriptori, simplement s'ha de fer clic a la fila de la taula. Els escriptoris seleccionats apareixeran com a marcats a la casella de "selected" al final de la fila. Encara que es navegui a la pàgina següent, els escriptoris seleccionats romandran marcats.

![Checkboxes de selecció](./domains.images/domains55.png)

Alternativament, si no se selecciona cap escriptori, es poden fer servir els filtres de dalt o de sota de la taula per filtrar-los.

!!! Warning Avís
    Si no se selecciona cap escriptori o apliqueu cap filtre, tots els escriptoris se seleccionaran per defecte.

Un cop s'hagin seleccionat els escriptoris, es tria una acció del desplegable per realitzar-la.

<!-- * Soft toggle started/shutting-down state:

* Toggle started/stopped state:

* Soft shut down:

* Shut down:

* Force Failed state:

* Delete:

* Start Paused (check status):

* Remove forced hypervisor:

* Remove favourite hypervisor: -->

### Edició en bloc

Es poden seleccionar diversos escriptoris fent clic a les files de la taula. De manera alternativa, es poden filtrar els escriptoris sense fer clic a les files i tots els escriptoris que es mostren a la taula comptaran com a seleccionats, igual que es fa amb Global Actions.

Al fer clic al botó ![Bulk Edit Desktops](./domains.images/domains40.png) apareixerà una finestra:

![(Finestra d'actualitzar múltiples escriptoris alhora)](./domains.images/domains41.png)

* Desktops to edit: Indica tots els escriptoris que s'actualitzaran amb les dades del formulari. Si hi ha molts escriptoris, es pot anar desplaçant per la llista.

* Desktops viewers: si està marcada, apareixerà una secció nova. Els visors dels escriptoris s'actualitzaran segons estigui seleccionat.

![Update viewers section](./domains.images/domains42.png)

Si està marcada l'opció "Update RDP credentials", també es canviarà el nom d'usuari i la contrasenya utilitzats per a les sessions RDP.

![(Update RDP credentials)](./domains.images/domains43.png)

* Hardware: les dades només es canviaran en els camps on el valor seleccionat és diferent de "--"

Si està marcada l'opció "Update network", apareixerà una secció nova. Les interfícies dels escriptoris seleccionades s'actualitzaran, fent servir la tecla CTRL mentre es fa clic a les opcions per a seleccionar més d'una interfície alhora.

![(Update network)](./domains.images/domains44.png)

* Bookable resources: Els reservables s'actualitzaran quan el valor seleccionat sigui diferent de "--"

Per a aplicar tots els canvis i actualitzar els escriptoris seleccionats, s'ha de fer clic al botó ![(Modify desktops)](./domains.images/domains45.png)

### Creació en bloc

Al fer clic a ![Bulk Add Desktops](./domains.images/domains56.png) apareixerà una finestra

![(Modal de Bulk Add)](./domains.images/domains57.png)

Aquest formulari permet crear diversos escriptoris simultàniament. Per crear-los, simplement s'ha d'omplir el formulari especificant un nom pels escriptoris nous i seleccionant la plantilla en què es basaran.

A la secció "Allowed", es poden seleccionar els usuaris pels quals es volen crear els escriptoris triant entre grups o noms d'usuari. Es crearà un escriptori separat per cada usuari seleccionat.

### Editar

Al fer clic al botó ![Edit](./domains.images/domains51.png) apareixerà una finestra. En aquesta finestra es poden editar els paràmetres de l'escriptori.

### XML

Al fer clic al botó ![XML](./domains.images/domains52.png) apareixerà una finestra on es pot modificar el fitxer de configuració de la màquina virtual [KVM/QEMU XML](https://libvirt.org/formatdomain.html).
