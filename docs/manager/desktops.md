# Desktops
To go to the "Desktops" section, press the button ![Administration](../admin/users.images/users1.png)

![Home page screen](../admin/users.images/users2.png)

And go to "Desktops"

![Administration sidebar](./domains.images/domains2.png)

In this section you can see all the desktops that have been created from all categories.

If you click the ![[+]](./domains.images/domains5.png) icon, you can see more information about the desktop.

* Status detailed info: displays the status of the desktop. It will show the error message if it failed, which can help identify the issue

* Description: description of the desktop

* Hardware: as its name indicates, it is the hardware that has been assigned to that desktop

* Storage: indicates the UUID of the disk associated and its use

* Media: indicates the media associtated with the desktop, if there is any

![Desktop details](./domains.images/domains6.png)

### Global actions

On top of the table we have a dropdown with several actions. These actions will be applied to all selected desktops.

![Global actions dropdown](./domains.images/domains54.png)

To select a desktop, simply click on the row in the table. The selected desktops will appear as checked in the checkbox at the end of the row. Even if you navigate to the next page, the selected desktops will remain checked.

![Selected checkboxes](./domains.images/domains55.png)

Alternatively, if you don't select any desktops, you can use the filters above or below the table to filter them.

!!! Warning
    If you don't select any desktop or apply any filters, ALL desktops will be selected by default.

Once you have selected the desktops, choose an action from the dropdown to perform it.

<!-- * Soft toggle started/shutting-down state:

* Toggle started/stopped state:

* Soft shut down:

* Shut down:

* Force Failed state:

* Delete:

* Start Paused (check status):

* Remove forced hypervisor:

* Remove favourite hypervisor: -->

### Bulk Edit Desktops

You can select multiple desktops by clicking on the rows on the table. Alternatively you can filter the desktops without clicking on the rows and all the desktops seen on the table will count as selected.

If you click the ![Bulk Edit Desktops](./domains.images/domains40.png) button a modal will appear:

![(Edit Multiple Desktops modal)](./domains.images/domains41.png)

* Desktops to edit: Indicates all the desktops that will be updated with the form data. If there are a lot of desktops you can scroll through the list.

* Desktops viewers: If it's checked, a new section will appear. The desktops' viewers will be updated as selected.

![(Update viewers section)](./domains.images/domains42.png)

If "Update RDP credentials" is checked, the username and password used for RDP sessions will be changed too.

![(Update RDP credentials)](./domains.images/domains43.png)

* Hardware: The data will be changed only on the fields where the value selected is other than "--"

If "Update network" is checked, a new section will appear. The desktops' interfaces will be updated as selected, using the CTRL key while clicking on the options to select more than one interface at once.

![(Update network)](./domains.images/domains44.png)

* Bookable resources: The resources will be updated when the value selected is other than "--"

To apply all changes and update the selected desktops, click the ![(Modify desktops)](./domains.images/domains45.png) button

### Bulk Add Desktops

If you click on ![Bulk Add Desktops](./domains.images/domains56.png) a modal will appear

![(Add Multiple Desktops modal)](./domains.images/domains57.png)

This form allows you to create multiple desktops simultaneously. To create them, simply fill out the form by specifying a name for the new desktops and selecting the template that they will be based on.

In the "Allowed" section, you can select the users you wish to create the desktops for by choosing from groups or usernames. A separate desktop will be created for each user selected.

### Edit

If you click the ![Edit](./domains.images/domains51.png) button a modal will appear. In this modal you can edit the desktop's parameters.

### XML

If you click the ![XML](./domains.images/domains52.png) button a modal will appear where you can modify the [KVM/QEMU XML](https://libvirt.org/formatdomain.html) configuration file of the virtual machine.
