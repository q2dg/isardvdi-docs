# Accés mitjançant OAuth

Es prem el botó ![](oauth_access.ca.images/codi3.png){width="8%"} de la pantalla principal d'IsardVDI.

![](local_access.ca.images/local_access1.png){width="80%"}

Demanarà un compte de correu de Google:

![](oauth_access.ca.images/google2.png){width="40%"}
![](oauth_access.ca.images/google3.png){width="40%"}


## Inscripció per codi de registre

S'introdueix el [codi d'autoregistre proporcionat](../manager/manage_user.ca.md/#clau-dautoregistre) per un administrador o gestor de la plataforma i es prem el botó ![](./oauth_access.ca.images/codi2.png){width="30%"}

![](oauth_access.ca.images/codi1.png){width="30%"}

I s'accedeix a la pàgina principal.

![](oauth_access.ca.images/home1.png){width="80%"}

!!! note "Nota"
    Una vegada autenticat l'usuari, **no** fa falta tornar a repetir el procés de registre. Al pròxim inici de sessió, només s'ha de prémer en el botó ![](oauth_access.ca.images/codi3.png){width="8%"} i utilitzar les **credencials de Google**.