# Direct Viewer Access

The user **advanced** or **manager** can [generate a link](../advanced/direct_viewer.md) so that a user can connect to a desktop viewer without having to be authenticated in the platform.

 This way, you only need to share the link to the direct viewer and enter it into the web browser. This will allow you to select a previously chosen viewer to access the desktop.

![](./direct_viewer_access.images/visor1.png)
