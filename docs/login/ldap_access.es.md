# Acceso LDAP

Si tu organización tiene un servidor LDAP i el  administrador de IsardVDI lo ha configurado, puedes acceder utilizando esas credenciales. El proceso es muy similar al acceso Local. Primero tienes que acceder a https://DOMINIO y seleccionar la categoría y el idioma:

![](./local_access.es.images/local_access1.png)

Si no aparece el seleccionador de categoría, no te preocupes. Puede que haya el acceso automático configurado, o bien has accedido a través de la URL de la categoría directamente:

![](./local_access.es.images/local_access2.png)

La elección del idioma se queda guardada en un cookie en el navegador, y la próxima vez que entres ya aparecerá preseleccionada con el idioma que elegiste la última vez.

## Inscripción por código de registro

Si es la primera vez que se accede como usuario aparecerá el formulario para introducir un código de registro. 

El código de registro se crea por parte del manager de la organización y se explica cómo se genera más adelante en la sección de "Manager" de la documentación. Cada usuario ha de pertenecer a un grupo y tener un rol, ya sea advanced (profesor), user (alumno), etc. Por cada grupo de usuarios se pueden generar códigos de autorregistro para diferentes roles.

Se introduce el código proporcionado por un Manager y se pulsa el botón ![](./oauth_access.es.images/codigo2.png)

![](./oauth_access.es.images/codigo1.png)

Y se accede a la interfaz básica autenticados con la cuenta de LDAP!

![](./oauth_access.es.images/home1.png)
