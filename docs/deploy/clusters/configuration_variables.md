# Configuration variables in clusters

In order to have diferent configuration variables in each cluster host with the same `isardvdi.cfg` for all hosts we have two systems:

* `/opt/isard-local/environment` is a file that loads variables at shell loading time. Therefore this variable content prevail over `isardvdi.cfg` variables, because the shell is loaded after `docker-compose`. These variable contents are loaded running the system using `docker-compose` and `pcs`.

    * `sh -i` loads `$ENV` file
        * <https://gitlab.com/isard/isardvdi/-/blob/38b6c520/docker/hypervisor/src/start.sh#L1>
        * <https://gitlab.com/isard/isardvdi/-/blob/38b6c520/docker/_common/haproxy-docker-entrypoint.sh#L1>

    * Export `ENV` environment variable
        * <https://gitlab.com/isard/isardvdi/-/blob/38b6c520/docker-compose-parts/hypervisor.yml#L24>
        * <https://gitlab.com/isard/isardvdi/-/blob/38b6c520/docker-compose-parts/video.yml#L26>
        * <https://gitlab.com/isard/isardvdi/-/blob/38b6c520/docker-compose-parts/monitor-proxy.yml#L24>

    * Mount `/opt/isard-local/environment` to `/usr/local/etc/environment`
        * <https://gitlab.com/isard/isardvdi/-/blob/38b6c520/docker-compose-parts/hypervisor.yml#L18>
        * <https://gitlab.com/isard/isardvdi/-/blob/38b6c520/docker-compose-parts/video.yml#L24>
        * <https://gitlab.com/isard/isardvdi/-/blob/38b6c520/docker-compose-parts/monitor-proxy.yml#L22>

* `/opt/isard/config/` can content files that are loaded by `pcs` `compose` resource before run `docker-compose`. Therefore this variable content prevail over `isardvdi.cfg`. These variable contents are not loaded using `docker-compose` without `pcs`.

    * `evaluate_env` function at `compose` `pcs` resource
        * <https://gitlab.com/isard/isardvdi/-/blob/38b6c520/sysadm/compose#L375>


