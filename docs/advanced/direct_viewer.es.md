# Visor directo

Se utiliza para compartir un escritorio mediante un enlace directo a éste, **sin necesidad** de que el usuario esté **registrado** a la plataforma.

Se pulsa el botón ![](direct_viewer.es.images/visor_directo3.png) del escritorio que se quiera compartir por enlace

![](./direct_viewer.es.images/visor_directo1.png){width="80%"}

Saldrá una ventana de diálogo donde se tiene que seleccionar la casilla para que aparezca el enlace y poder copiarlo

![](./direct_viewer.es.images/visor_directo2.png){width="80%"}

