# Domains

!!! info "Role access"

    Only **administrators** have access to all this features.

To go to the "Domains" section, press the button ![](./users.images/users1.png)

![](./users.images/users2.png)

And press the dropdown menu ![](./domains.images/domains1.png)

![](./domains.images/domains2.png)


## Desktops

In this section you can see all the desktops that have been created from all categories.

![](./domains.images/domains4.png)

If you click the ![](./domains.images/domains5.png) icon, you can see more information about the desktop.

* Hardware: as its name indicates, it is the hardware that has been assigned to that desktop

* Template tree: a visual representation that shows the hierarchy of desktops and the template where this desktop derivates and its dependencies. This is useful to understand the origin of a particular desktop

* Storage: indicates the UUID of the disk associated and its use

* Media: indicates the media associtated with the desktop, if there is any

![](./domains.images/domains6.png)

### Global actions

On top of the table we have a dropdown with several actions. These actions will be applied to all selected desktops.

![Global actions dropdown](./domains.images/domains54.png)

To select a desktop, simply click on the row in the table. The selected desktops will appear as checked in the checkbox at the end of the row. Even if you navigate to the next page, the selected desktops will remain checked.

![Selected checkboxes](./domains.images/domains55.png)

Alternatively, if you don't select any desktops, you can use the filters above or below the table to filter them.

!!! Warning
    If you don't select any desktop or apply any filters, ALL desktops will be selected by default.

Once you have selected the desktops, choose an action from the dropdown to perform it.

<!-- * Soft toggle started/shutting-down state:

* Toggle started/stopped state:

* Soft shut down:

* Shut down:

* Force Failed state:

* Delete:

* Start Paused (check status):

* Remove forced hypervisor:

* Remove favourite hypervisor: -->

### Bulk edit desktops

You can select multiple desktops by clicking on the rows on the table. Alternatively you can filter the desktops without clicking on the rows and all the desktops seen on the table will count as selected.

If you click the ![Bulk Edit Desktops](./domains.images/domains40.png) button a modal will appear:

![(Edit Multiple Desktops modal)](./domains.images/domains41.png)

* Desktops to edit: Indicates all the desktops that will be updated with the form data. If there are a lot of desktops you can scroll through the list.

* Desktops viewers: If it's checked, a new section will appear. The desktops' viewers will be updated as selected.

![(Update viewers section)](./domains.images/domains42.png)

If "Update RDP credentials" is checked, the username and password used for RDP sessions will be changed too.

![(Update RDP credentials)](./domains.images/domains43.png)

* Hardware: The data will be changed only on the fields where the value selected is other than "--"

If "Update network" is checked, a new section will appear. The desktops' interfaces will be updated as selected, using the CTRL key while clicking on the options to select more than one interface at once.

![(Update network)](./domains.images/domains44.png)

* Bookable resources: The resources will be updated when the value selected is other than "--"

To apply all changes and update the selected desktops, click the ![(Modify desktops)](./domains.images/domains45.png) button

### Bulk Add Desktops

If you click on ![Bulk Add Desktops](./domains.images/domains56.png) a modal will appear

![(Add Multiple Desktops modal)](./domains.images/domains57.png)

This form allows you to create multiple desktops simultaneously. To create them, simply fill out the form by specifying a name for the new desktops and selecting the template that they will be based on.

In the "Allowed" section, you can select the users you wish to create the desktops for by choosing from roles, categories, groups or usernames. A separate desktop will be created for each user selected.

## Templates

In this section you can see all the templates that have been created from all categories.

![](./domains.images/domains7.png)

If you click on the ![](./domains.images/domains5.png) icon, you can see more information about the template.

* Hardware: as its name indicates, it is the hardware that has been assigned to that template

* Allows: indicates who has been given permission to use the template

![](./domains.images/domains8.png)

<!-- ### Templates options

#### Forced Hypervisor
 -->

### Enable

Enabling a template allows all users with whom the template is shared to view and use it for creating desktops.

To enable or disable a template, simply click on the checkbox in the "Enabled" column of the table and confirm your selection in the notification.

![Enabled checkboxes](./domains.images/domains53.png)

If you click on the ![Hide Disabled](./domains.images/domains49.png) button located in the top right corner, the templates that are not enabled (visible to the users it's shared with) will not be shown in the table. When clicking on the button ![View Disabled](./domains.images/domains50.png) again they will be displayed.
#### Duplicate

This option allows you to duplicate in database the template while keeping the original template disk.

Template duplication can be performed multiple times, and is useful for various scenarios, such as:

- You want to share template within other categories. You can duplicate the template and assign it to another user in another category with separate sharing permissions for the new copy while maintaining the original version

Deleting a duplicated template will not affect the original template.

When duplicating a template, you can customize the new copy:

- Name of the new template
- Description of the new template
- User assigned as the new owner of the template
- Enable or disable visibility of the template to shared users
- Select users to share the template with

![](./domains.images/domains46.png)

#### Delete

This option will delete the domain and its disk.

![](./domains.images/domains47.png)

When deleting a template, a modal window will appear displaying all desktops created from it and any templates duplicated. Deleting the template will result in the deletion of all associated domains and their disks.

![](./domains.images/domains48.png)

However, if the template to delete is duplicated from another template, there is no need to delete them all unless the origin template is deleted as well.

#### Edit

If you click the ![Edit](./domains.images/domains51.png) button a modal will appear. In this modal you can edit the desktop's parameters.

#### XML

If you click the ![XML](./domains.images/domains52.png) button a modal will appear where you can modify the [KVM/QEMU XML](https://libvirt.org/formatdomain.html) configuration file of the virtual machine.

## Media

In this section you can see all the media files that have been uploaded from all categories.

![](./domains.images/domains9.png)


## Resources

In this section you can see all the resources that the installation has, these will appear when creating a desktop/template.

![](./domains.images/domains10.png)


### Graphics

In this section you can add different types of viewers with which you can see a desktop.

To add a new viewer, click the button ![](./domains.images/domains11.png)

![](./domains.images/domains12.png)

And the fill out the form

![](./domains.images/domains13.png)


### Videos 

In this section you can add different video formats.

To add a new format, press the button ![](./domains.images/domains11.png)

![](./domains.images/domains14.png)

And the fill out the form

![](./domains.images/domains15.png)

(heads -> cuantos monitores quieres)

### Interfaces

In this section you can add private networks to desktops.

To add a network, press the button ![](./domains.images/domains11.png)

![](./domains.images/domains16.png)

And the fill out the form

* Type: Bridge , Network, OpenVSwitch, Personal

    - Bridge: It links with a bridge to a server network. On the server you can have interfaces that link, for example, with vlans to a backbone of your network, and you can map these interfaces in isard and connect them to the desktops.

    In order to map the interface within the hypervisor, this line must be modified in the isardvdi.conf file:

    ```
    # ------ Trunk port & vlans --------------------------------------------------
    ## Uncomment to map host interface name inside hypervisor container.
    ## If static vlans are commented then hypervisor will initiate an 
    ## auto-discovery process. The discovery process will last for 260
    ## seconds and this will delay the hypervisor from being available.
    ## So it is recommended to set also the static vlans.
    ## Note: It will create VlanXXX automatically on webapp. You need to
    ## assign who is allowed to use this VlanXXX interfaces.
    #HYPERVISOR_HOST_TRUNK_INTERFACE=

    ## This environment variable depends on previous one. When setting
    ## vlans number comma separated it will disable auto-discovery and
    ## fix this as forced vlans inside hypervisor.
    #HYPERVISOR_STATIC_VLANS=
    ```


* Model: rtl8931, virtio, e1000

    - It is more efficient to use virtio (which is a paravirtualized interface), while the e1000 or rtl are card simulations, and it is slower, although it is more compatible with older operating systems and it is not necessary to install drivers in the case of windows.

    **If you have a modern operating system, it works with virtio, but with the others that are interfaces. Old windows: rtl, e1000**


* QoS: limit up and down to 1 Mbps, unlimited

![](./domains.images/domains17.png)


### Boots

This section lists the different boot modes for a desktop.

**If you do not have permissions, you cannot select a bootable iso and it is not allowed by default that users can map isos.**

To give permissions, click on the icon ![](./domains.images/domains25.png)

![](./domains.images/domains18.png)


### Network QoS

This section lists the limitations that can be placed on networks.

To add a limitation, click the button ![](./domains.images/domains11.png)

![](./domains.images/domains19.png)

And the fill out the form

![](./domains.images/domains20.png)


### Disk QoS

This section lists the limitations that can be placed on disks.

To add a disk limit, click the button ![](./domains.images/domains11.png)

![](./domains.images/domains21.png)

And the fill out the form

![](./domains.images/domains22.png)


### Remote VPNs

In this section you can add remote networks to desktops.

To add a remote network, press the button ![](./domains.images/domains11.png)

![](./domains.images/domains23.png)

And the fill out the form

![](./domains.images/domains24.png)


## Bookables



### Priority

In this section you can create priorities depending on what is needed at that time. Here you can create rules with a certain maximum reservation time, number of desks, etc.

![](./domains.images/domains26.png)

To add a new priority, press the button ![](./domains.images/domains11.png)

![](./domains.images/domains27.png)

And a dialog box will appear where you can fill in the fields, also allows you to share with other roles, categories, groups or users:

* **Name**: The name that you want to give to the priority

* **Description**: A short description to know a little more information about that priority

* **Rule**: It is the rule that we assign to that priority, this is applied in the "Resources" section. It is important to note that if you want to apply more than one rule to a priority, you need to name them the same.

* **Priority**: It is the priority given to the rule; the higher the number, the more priority it will have and the lower the number, the less it will have.

* **Forbid time**: It is the time in advance that reservations cannot be made.

* **Max time**: It is the maximum time of a reservation.

* **Max items**: These are the maximum number of desks allowed to have this rule.

![](./domains.images/domains28.png)


#### Share

If you want to share a priority with a role, category, group or user, click on the icon ![](./domains.images/domains29.png)

![](./domains.images/domains30.png)

And fill out the form

![](./domains.images/domains31.png)


#### Edit

To edit a priority, click the icon ![](./domains.images/domains32.png)

![](./domains.images/domains33.png)

A dialog box appears where you can edit the fields:

![](./domains.images/domains34.png)


### Resources

In this section you can find all the GPU profiles that have been selected in the "Hypervisors" section

Here you can apply the priorities/rules that have been previously created.

![](./domains.images/domains35.png)


#### Share

To share a resource, click the icon![](./domains.images/domains29.png)

![](./domains.images/domains36.png)

And fill out the form

![](./domains.images/domains37.png)


#### Edit

To edit a resource, click the icon ![](./domains.images/domains32.png)

![](./domains.images/domains38.png)

A dialog box appears where you can edit the fields:

![](./domains.images/domains39.png)


### Events 

In this section you can see the events that are generated derived from the actions of the gpus.



