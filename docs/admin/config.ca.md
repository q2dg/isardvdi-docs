# Configuració

!!! info "Rols amb accés"

    Només els **administradors** tenen accés a aquesta característica.

Per a anar a la secció de "Config", es prem el botó ![](./users.ca.images/users1.png)

![](./users.ca.images/users2.png)

I es prem el botó ![](./config.images/config1.png)

![](./config.images/config2.png)


## Programador de tasques

En este apartado se puede programar un "trabajo", puede ser una copia de seguridad o 

* Type: Cron (que haga el trabajo a una hora determinada), interval (que haga el trabajo cada x tiempo)

* Hour: indica la hora

* Minute: indica el minuto

* Action: backup database, check ephimeral domain status

## Mode manteniment

El mode de manteniment desactiva les interfícies web per als usuaris amb rols de managers, avançats i d'usuari.

L'administrador del sistema pot crear un arxiu anomenat `/opt/isard/config/maintenance` per a canviar a el mode de manteniment a l'inici. L'arxiu s'elimina una vegada que el sistema canvia a el mode de manteniment.

El mode de manteniment es pot habilitar i deshabilitar a través de la secció de [configuració](./config.ca.md) del panell d'administració per part d'un usuari amb funció d'administrador.

Per a habilitar i deshabilitar aquesta funció, es marca la casella

![](./config.images/config6.png)

Una vegada marcada la casella, quan els usuaris intentin iniciar sessió al seu compte, els apareixerà un missatge de manteniment

![](./config.images/config7.png)

![](./config.images/config8-ca.png)
