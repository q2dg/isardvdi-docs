# Users

You can add users in the `Default` group that exists on fresh install. Also you can create new categories and groups.

NOTES:

1. Manager roles are tied to a category when created. This implies that this manager will have **full rights** in its category. Will be able to do anything with desktops, templates and media in its category, even if they are from the admin. This has to be taken into account specially when creating a manager in the 'default' category (where the admin is).
2. A manager will be able to create groups and users but only within its category.

## Administer and create Users
### Create categories

Press the button ![](./users.images/users1.png) to got to the Administration panel

![](./users.images/users2.png)

In the "Users" section, look where it says "Categories" and press the button ![](./users.images/users3.png)

![](./users.images/users4.png)

A dialog box will appear where the form fields can be filled.

![](./users.images/users5.png)

There are three options that can be applied when creating a category:

* Frontend dropdown show: That on the login page, the category created appears in the drop-down menu

    ![](./users.images/users6.png)

    If the category is hidden it is still possible to login in that category using the url `/login/<category_id>`

* Auto desktops creation: A desktop will be created when a user logs in with the chosen template (in this example with "Windows 10 PRO base")

    ![](./users.images/users7.png)

* Ephemeral desktops: To be able to put a limited time of use to a temporary desk. (For this you also have to configure the "Job Scheduler" in "Config" section)

    ![](./users.images/users8.png)

### Create groups

Similar to creating a category you can create multiple groups in each category.

### Create individual users

You can create individual users and select it's category and group and set the role. This will imply the quotas and limits applied to the user.

You can also add `Secondary groups` to users. A user will get access to all resources in system shared with it's primary group and all secondary groups.

### Create/Update users from CSV

You can create users in bulk by uploading a CSV with your users.

![upload csv](./users.images/bulk1.png)

To fill correctly the csv you can download the template and follow instructions in form. An alert will show you if there are any errors that must be fixed before processing the CSV file.

Once the file is correct it will show contents processed in a table and allow to add `secondary groups` and set quotas if required.

![verify csv](./users.images/bulk2.png)

The fields in grey columns are not updated (when the user already exists).

You can check the 'Update existing users' to also update the one's found already in database.

## Access via OAuth

To be able to access through Google from an external account, go to the "Administration" panel in the "Users" section.

And in the "Categories" section, press the button ![](./users.images/users9.png) of the category to which we want to give access.

![](./users.images/users10.png)

Press the button ![](./users.images/users11.png)

![](./users.images/users12.png)

And a dialog window with a form appears. The "Allowed email domain for foreign account like Google" field is filled in with the email domain.

![](./users.images/users13.png)

## Options

When you open a user details you'll found options to:

- **Impersonate**: your session will be swapped to the user you impersonate as.
- **Delete**: will delete user along with all their desktops, media and templates. Be careful as other uses could have created desktops from this user templates. This desktops will also be deleted as it's parent template is being deleted.

  To avoid this, duplicate template as this will keep the disk.

- **Edit**: edit user
- **Reset password**: will only work with local users.
- **Enable/Disable**: will keep this user (and all it's resources) in system when disabled, but won't be able to log in.
