# Lanzamientos

El flujo de desarrollo del IsardVDI es *rolling release*, publicaciones continuas.

En un modelo de desarrollo de publicaciones continuas, el desarrollo de software está en curso y las nuevas actualizaciones se publican continuamente tan pronto como estén preparadas. En lugar de esperar una fecha de lanzamiento específica para introducir nuevas características y mejoras, las actualizaciones se publican tan pronto como se desarrollen y se pongan a prueba, permitiendo a los usuarios estar actualizados con la última versión del software en todo momento.

En un modelo de desarrollo de publicaciones continuas, no hay números de versión específicos o lanzamientos importantes como en un modelo de desarrollo de software tradicional. En cambio, las nuevas actualizaciones se identifican por la fecha o el número de construcción, y los usuarios pueden actualizar a la última versión en cualquier momento.

Este enfoque permite actualizaciones más frecuentes y una entrega más rápida de nuevas características y correcciones de errores, pero también requiere más pruebas y control de calidad para asegurar que las nuevas actualizaciones no introduzcan problemas nuevos o rompan la funcionalidad existente. También requiere que los usuarios estén informados sobre mantener su software actualizado, puesto que las nuevas actualizaciones pueden requerir configuración adicional o ajustes para funcionar correctamente.

Por lo tanto, ahora podéis seguir las actualizaciones nuevas a la rama principal a [https://gitlab.com/isard/isardvdi/-/commits/main](https://gitlab.com/isard/isardvdi/-/releases)