# Reservar escriptori

Abans de crear una reserva per primer cop, s'ha de llegir l'[apartat de reserves del manual](../../user/bookings.ca) on s'explica millor què són les reserves i per què són necessàries amb escriptoris amb GPU.

## Crear una reserva

S'accedeix a reservar una GPU per l'escriptori des de la última icona d'accions d'aquest.

![](book_desktop.ca.images/1.png){width="80%"}

Dóna accés a la vista setmanal de la **disponibilitat** (es pot canviar la vista a mensual o diària), on es troben dues columnes per cada dia de la setmana. En la columna de l'esquerra surt la disponibilitat pel perfil de targeta, i en la columna de la dreta és on sortiran les reserves que hi han per a aquest escriptori.

En la figura s'aprecia que existeix **disponibilitat** durant la setmana i no hi ha feta **cap** reserva.

![](book_desktop.ca.images/2.png){width="80%"}

Les reserves poden crear-se mitjançant el botó de dalt a la dreta ![](book_desktop.ca.images/3.png) o mitjançant el cursor, **clicant en la franja Reserves i arrossegant** per a seleccionar el rang d'hora desitjat. En el formulari que apareixerà s'ajusten el rang de dates i hores de durada de la reserva que volem realitzar.

![](book_desktop.ca.images/4.png){width="60%"}

Una vegada feta la reserva, sortirà a la columna de la dreta de cada jornada. 

![](book_desktop.ca.images/5.png){width="20%"}


## Esborrar reserva

Per a eliminar una reserva, es fa clic sobre la franja i mitjançant el botó d'eliminació que surt al editar-la.

![](book_desktop.ca.images/6.png){width="60%"}
![](book_desktop.ca.images/7.png){width="30%"}

![](book_desktop.ca.images/8.png){width="20%"}


## Videotutorials

Aquí oferim dos videotutorials que expliquen les configuracions prèvies d'un escriptori o desplegament, i com realitzar reserves per ambdues opcions, amb els **dos mètodes de reserva que existeixen a IsardVDI**, a la primera part i segona part respectivament. 

Subtítols en català disponibles.

<iframe width="560" height="315" src="https://www.youtube.com/embed/9ZnSCFQ7I_s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/Tvkd4OE26y4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
