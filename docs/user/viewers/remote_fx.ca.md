# RemoteFX - Com escanejar dispositius mitjançant el visor RDP

## Descripció

RemoteFX és una tecnologia desenvolupada per Microsoft que permet la virtualització de gràfics i recursos de maquinari en entorns d'escriptori remot. Està dissenyat per millorar l'experiència de l'usuari proporcionant una qualitat gràfica excepcional i un rendiment sòlid en entorns d'escriptori virtualitzats.

RemoteFX està estretament relacionat amb l'ús de Remote Desktop Protocol (RDP) en l'escriptori remot de Windows. RDP és un protocol de Microsoft que permet als usuaris accedir i controlar de forma remota un ordinador o un servidor des d'un altre dispositiu. RemoteFX amplia les capacitats de RDP afegint funcionalitats de virtualització de gràfics i USB.

Pel que fa a l'escaneig de dispositius mitjançant RDP en l'escriptori remot de Windows, RemoteFX permet que els dispositius USB connectats a la màquina local s'escanegin i estiguin disponibles en la sessió de l'escriptori remot. Això significa que els usuaris poden utilitzar dispositius USB com ara impressores, escàners, càmeres web, dispositius d'emmagatzematge i altres perifèrics directament des de l'escriptori remot, com si estiguessin connectats localment.

Pel què fa al visor RDP, RemoteFX és una tecnologia clau per habilitar l'escaneig de dispositius USB en els escriptoris virtuals. Permet que els usuaris que accedeixen a través del visor RDP en els escriptoris virtuals utilitzin i accedeixin als dispositius USB connectats a la seva màquina local des de l'entorn virtualitzat.


## Com implementar RemoteFX

A l'**equip amfitrió** i l'**escriptori virtual** (si és un sistema operatiu Windows), s'han d'implementar les següents polítiques de grup.

!!! Tip
    Si l'escriptori virtual té un sistema operatiu Windows, les **plantilles predefinides que ofereix IsardVDI** ja vénen **preparades amb les configuracions necessàries que requereix RemoteFX**, que s'expliquen a continuació. Per tant, només cal aplicar aquesta configuració a l'**equip amfitrió** si l'usuari utilitza aquestes plantilles per crear escriptoris.

    En cas contrari, si l'usuari ha creat un **escriptori personalitzat de Windows per compte propi**, aquí s'explica com configurar el sistema per poder escanjear dispositius USB estranys mitjançant el protocol RDP a Windows.

1. **Obre l'eina "Editar política de grup"**

    ![](remote_fx.es.images/directiva_de_grupo.png){ width="30%" }

2. **Configuració de l'equip >> Plantilles administratives >> Components de Windows >> Serveis d'Escriptori Remot**

    ![](remote_fx.es.images/servicios_escritorio_remoto.png){ width="60%" }

3. **Client de connexió a Escriptori Remot**

    1. **Redirecció de dispositius USB RemoteFX >> Permetre la redirecció RDP d'altres dispositius USB RemoteFX compatibles des d'aquest dispositiu**

        Cal habilitar la política i aplicar-la per a tots els usuaris:

        ![](remote_fx.es.images/directiva1.png)

        ![](remote_fx.es.images/habilitar_directiva.png){ width="60%" }

4. **Host de sessió d'escriptori remot**

    1. **Entorn de sessió remota >> RemoteFX per a Windows Server 2008 R2 >> Configurar RemoteFX**

        Cal habilitar la política:

        ![](remote_fx.es.images/directiva_configurar_remote_fx.png)

    2. **Redirecció de dispositiu o recurs**

        Cal habilitar i deshabilitar les següents polítiques:

        **Habilitar**

          * Permetre la redirecció de la reproducció d'àudio i vídeo
          * Permetre la redirecció de la gravació d'àudio
          * Permetre la redirecció de la zona horària
          * Permetre la redirecció de l'automatització de la interfície d'usuari

        **Deshabilitar**

          * No permetre la redirecció de la captura de vídeo
          * No permetre la redirecció del Portapapers
          * No permetre la redirecció de ports COM
          * No permetre la redirecció d'unitats
          * No permetre la redirecció de ports LPT
          * No permetre la redirecció de dispositius Plug and Play compatibles
          * No permetre la redirecció de la ubicació
          * No permetre la redirecció de dispositius de targeta intel·ligent
          * No permetre la redirecció de WebAuth

        ![](remote_fx.es.images/host_lista_directivas.png){ width="80%" }


Un cop fet tot això, es recomana **reiniciar l'equip i l'escriptori** perquè els canvis s'apliquin correctament.

!!! Tip
    El següent pas només és necessari si el **sistema operatiu de l'escriptori virtual és Windows** i l'escriptori no ha derivat d'una de les **plantilles predefinides que ofereix IsardVDI**, on **ja es troben aquestes configuracions**.

1. A l'escriptori virtual, obre una línia de comandes CMD i executa ``gpupdate /force`` i **reinicia l'escriptori**.

    ![](remote_fx.es.images/gpupdate_force.png){width="60%"}


2. A l'amfitrió, edita el fitxer descarregat ***.rdp*** del **visor** de l'escriptori.

    ![](viewers.es.images/visor_rdp_14.png) 

    1. **Recursos locals >> Més**

        Es pot comprovar el **dispositiu detectat per l'equip** i preparat per **escanejar-lo dins de l'escriptori**.

        ![](viewers.es.images/visor_rdp_15.png){width="40%"}
        ![](viewers.es.images/visor_rdp_16.png){width="40%"}

        ![](viewers.es.images/visor_rdp_17.png){width="40%"}
        ![](viewers.es.images/visor_rdp_18.png){width="40%"}