# RemoteFX - How to scan devices via RDP viewer

## Description

RemoteFX is a technology developed by Microsoft that enables virtualization of graphics and hardware resources in remote desktop environments. It is designed to enhance the user experience by providing exceptional graphics quality and robust performance in virtualized desktop environments.

RemoteFX is closely related to the use of Remote Desktop Protocol (RDP) in Windows remote desktop. RDP is a Microsoft protocol that allows users to remotely access and control a computer or server from another device. RemoteFX extends the capabilities of RDP by adding graphics and USB virtualization functionality.

As for device scanning via RDP on the remote Windows desktop, RemoteFX allows USB devices connected to the local machine to be scanned and made available in the remote desktop session. This means that users can use USB devices such as printers, scanners, webcams, storage devices and other peripherals directly from the remote desktop, as if they were connected locally.

Related to the RDP viewer, RemoteFX is a key technology for enabling scanning of USB devices on virtual desktops. It allows users accessing through the RDP viewer on virtual desktops to use and access USB devices attached to their local machine from the virtualized environment.


## How to deploy RemoteFX

On **host machine** and **virtual desktop** (if Windows O.S.) the following group policies must be implemented.

!!! Tip
    If the virtual desktop has a Windows O.S., the **prebuilt templates provided by IsardVDI** already come **prepared with the necessary settings needed by RemoteFX** explained below, so only apply these settings on the **host computer** if the user uses these templates to create desktops.

    Otherwise, if the user has created a **custom Windows desktop on his own**, here is how to configure his system to be able to scan foreign USB devices via RDP protocol in Windows. 

1. **Open "Edit Group Policy"** tool.

    ![](remote_fx.es.images/directiva_de_grupo.png){ width="30%" }

2. **Computer Configuration >> Administrative Templates >> Windows Components >> Remote Desktop Services**

    ![](remote_fx.es.images/servicios_escritorio_remoto.png){ width="60%" }

3. **Remote Desktop connection client**

    1. **USB RemoteFX device redirection >> Allow RDP redirection of other supported USB RemoteFX devices from this device**

        The policy must be enabled and applied for all users:

        ![](remote_fx.es.images/directiva1.png)

        ![](remote_fx.es.images/habilitar_directiva.png){ width="60%" }

4. **Remote desktop session host**

    1. **Remote session environment >> RemoteFX for Windows Server 2008 R2 >> Configure RemoteFX**

        The policy must be enabled:

        ![](remote_fx.es.images/directiva_configurar_remote_fx.png)

    2. **Device or resource redirection**

        The following directives must be enabled and disabled:

        **Enable**

        * Allow audio and video playback redirection
        * Allow audio recording redirection
        * Allow time zone redirection
        * Allow user interface automation redirection

        **Disable**

        * Disallow video capture redirection
        * Disallow Clipboard redirection
        * Do not allow COM port redirection
        * Do not allow drive redirection
        * Do not allow LPT port redirection
        * Do not allow Plug and Play compatible device redirection
        * Do not allow location redirection
        * Do not allow smart card device redirection
        * Do not allow WebAuth redirection

        ![](remote_fx.es.images/host_lista_directivas.png){ width="80%" }

Once all this is done, it is recommended to **reboot computer and desktop** for the changes to be applied correctly.

!!! Tip
    The following step is only necessary if the **virtual desktop operating system is Windows**, and the desktop has not been derived from one of the **prebuilt templates offered by IsardVDI**, where **these settings** are already present.

1. On the virtual desktop you open a CMD command line and run ``gpupdate/force`` and **reboot the desktop**.

    ![](remote_fx.es.images/gpupdate_force.png){width="60%"}


2. On the host, edit the downloaded ***.rdp*** file of the desktop **viewer**.

    ![](viewers.es.images/visor_rdp_14.png){width="50%"}

    1. **Local Resources >> More**.

        You can check the **device detected by the computer** and ready to **scan it into the desktop**.

        ![](viewers.es.images/visor_rdp_15.png){width="40%"}
        ![](viewers.es.images/visor_rdp_16.png){width="40%"}

        ![](viewers.es.images/visor_rdp_17.png){width="40%"}
        ![](viewers.es.images/visor_rdp_18.png){width="40%"}