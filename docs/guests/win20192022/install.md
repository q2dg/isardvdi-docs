# Windows Server 2022 and 2019 installation guide

## Pre-installation

1. Download Windows Server [2019](https://www.microsoft.com/en-us/evalcenter/download-windows-server-2019) or [2022](https://www.microsoft.com/en-us/evalcenter/download-windows-server-2022) ISO image

2. [Generate a desktop from that media](../../../advanced/media/#generate-desktop-from-media) with the following virtual hardware:

    - Viewers: **SPICE**
    - vCPUS: **4**
    - Memory (GB): **8**
    - Videos: **Default**
    - Boot: **CD/DVD**
    - Disk Bus: **Default**
    - Disk size (GB): **100**
    - Networks: **Default**
    - Template: **Microsoft windows 10 with Virtio devices UEFI**

3. [Edit desktop and assign more Media](../../../user/edit_desktop/#media), so that it will end up with the following:

    - **WinServer2022 - ES** (installer)
    - **virtio-win-X** (drivers)
    - **Optimization Tools** (optimization software for Windows O.S.)


## Installation

1. **Send "Ctr+Alt+Supr"** and press any keyboard key on second screen

    ![](install.es.images/pantalla1.png){width="45%"}
    ![](install.es.images/send_key.png){width="13%"}
    ![](install.es.images/pantalla2.png){width="45%"}

2. Installation type

    ![](install.images/1-eng.png){width="45%"}
    ![](install.images/2-eng.png){width="45%"}

    ![](install.images/3-eng.png){width="45%"}
    ![](install.images/4-eng.png){width="45%"}

3. Load operating system drivers

    ![](install.images/5-eng.png){width="45%"}
    ![](install.images/6-eng.png){width="45%"}

    ![](install.images/7-eng.png){width="45%"}
    ![](install.images/8-eng.png){width="45%"}

    ![](install.images/9-eng.png){width="45%"}
    ![](install.images/10-eng.png){width="45%"}

    ![](install.images/11-eng.png){width="45%"}
    ![](install.images/12-eng.png){width="45%"}


## Configuration

1. Shut-down the desktop and [edit it](../../../user/edit_desktop) with the following virtual hardware:

    - Viewers: **RDP** and **Browser RDP**
    - Login RDP:
        - User: **Administrator**
        - Password: **Aneto_3404**
    - vCPUS: **4**
    - Memory (GB): **8**
    - Videos: **Default**
    - Boot: **Hard Disk**
    - Disk Bus: **Default**
    - Networks: **Default** and **Wireguard VPN**

2. Start the desktop, it will take a few seconds to start the operating system

    ![](install.images/13-eng.png){width="45%"}

3. Assign **Aneto_3404** password to **Administrator** user

    ![](install.images/14-eng.png){width="45%"}
    ![](install.images/15-eng.png){width="45%"}

4. Install **virtio** drivers from assigned desktop Media. Both installations are fast and have to only press **Next** button. 

    ![](install.images/16-eng.png){width="45%"}
    ![](install.images/17-eng.png){width="45%"}

    ![](install.images/18-eng.png){width="45%"}
    ![](install.images/19-eng.png){width="45%"}

5. Enable **Remote Desktop** connections. Enable first checkbox and disable second one, at **Advanced settings**

    ![](install.images/20-eng.png){width="20%"}
    ![](install.images/21-eng.png){width="38%"}
    ![](install.images/22-eng.png){width="39%"}

6. Check **system updates**, which will take a long time to download and install, in order to be up to date with the latest version of Windows Server (it may be necessary to reboot the system several times to check for new updates).

    ![](install.images/23-eng.png){width="60%"}


## Optimization tools

1. Run file from desktop assigned media

    ![](install.images/24-eng.png){width="45%"}

2. Press button **Analyze** and then **Common options**

    ![](install.images/25-eng.png){width="45%"}
    ![](install.images/26-eng.png){width="45%"}

3. Configure options as in images below

    ![](install.images/27-eng.png){width="45%"}
    ![](install.images/28-eng.png){width="45%"}

    ![](install.images/29-eng.png){width="45%"}
    ![](install.images/30-eng.png){width="45%"}

    ![](install.images/31-eng.png){width="45%"}
    ![](install.images/32-eng.png){width="45%"}

    ![](install.images/33-eng.png){width="45%"}
    ![](install.images/34-eng.png){width="45%"}

4. (**IF CATALAN LANGUAGE IS SET, ELSE, SKIP**) Filter by the word **idioma** and deactivate the following 3 options.

    ![](../win10/install.es.images/58.png){width="45%"}

5. Press button **Optimize** and wait until second screen shows up with result resume

    ![](install.images/36-eng.png){width="45%"}

6. Reboot the system
