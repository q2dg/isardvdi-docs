# Ubuntu Server 22.04 Jammy Jellyfish install

## Instal·lació del SO

![](installation.images/3.png){width="45%"}
![](installation.images/4.png){width="45%"}


## Isard (rols admin o manager)

**Modificar XML de l'escriptori**

![](../desktop/installation/images/modify_xml.png){width="40%"}

```
<driver name="qemu" type="qcow2" cache="unsafe" discard="unmap"/>
```


## Configuració

**Comandaments bàsic**
```
$ sudo apt update
$ sudo apt upgrade -y
$ sudo apt install vim net-tools htop curl wget spice-vdagent qemu-guest-agent
```

**Modificar *fstab***
```
$ sudo vim /etc/fstab
for every "ext4" storage, define "noatime,discard"

UUID=xxxxx-xxxxx-xxxxxx /               ext4    defaults,noatime,discard,errors=remount-ro 0       1
```

**Alliberar espai al sistema**
```
$ cd /
$ sudo fstrim -a -v 
```

**Reduir la memòria swap**
```
$ sudo sysctl vm.swappiness=1
```

**Reduir emmagatzematge de logs**
```
$ sudo vim /etc/systemd/journald.conf
SystemMaxUse=20M
SystemKeepFree=4G
```

**Deshabilitar actualitzacions automàtiques**
```
$ sudo apt purge update-manager update-notifier*
$ sudo vim /etc/apt/apt.conf.d/50unattended-upgrades
Unattended-Upgrade::DevRelease "false";
$ sudo vim /etc/update-manager/release-upgrades
Prompt=never
$ sudo snap refresh --hold 
$ sudo snap set system refresh.retain=3
```